package main


import (
	"os"
	"os/exec"
	"log"
	"net/http"
)

var cmd string
var port string

func run(w http.ResponseWriter, r *http.Request) {
	c := exec.Command(cmd)
	c.Stdout = w
	//io.WriteString(w, "Hello world!")
	err := c.Run()
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	cmd = os.Args[1]
	port = os.Args[2]
	http.HandleFunc("/", run)
	http.ListenAndServe(":" + port, nil)
}

